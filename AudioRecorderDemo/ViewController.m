//
//  ViewController.m
//  AudioRecorderDemo
//
//  Created by James Cash on 14-07-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
@import AVFoundation;

@interface ViewController ()

@property (nonatomic,strong) NSURL *audioFileURL;

@property (nonatomic,strong) AVAudioRecorder *recorder;

@property (nonatomic,strong) AVAudioPlayer* player;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    NSString* docsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSLog(@"Our docs directory is %@", docsDir);
    self.audioFileURL = [NSURL URLWithString:[docsDir stringByAppendingPathComponent:@"audio.m4a"]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toggleRecord:(UIButton*)sender {
    if (self.recorder.isRecording) {
        [self.recorder stop];
        [sender setTitle:@"Record" forState:UIControlStateNormal];
    } else {

        // request permission to record
        [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
            if (granted) {
                [sender setTitle:@"Stop" forState:UIControlStateNormal];

                NSError *err = nil;
                self.recorder = [[AVAudioRecorder alloc]
                                 initWithURL:self.audioFileURL
                                 settings:@{AVFormatIDKey: @(kAudioFormatMPEG4AAC),
                                            AVSampleRateKey: @(44100),
                                            AVNumberOfChannelsKey: @(2)}
                                 error:&err];

                if (err != nil) {
                    NSLog(@"Couldn't create recorder: %@",
                          err.localizedDescription);
                    abort();
                }
                
                [self.recorder record];
            } else {
                NSLog(@"No permisison to record");
            }
        }];

    }
}

- (IBAction)togglePlay:(UIButton*)sender {
    if (self.player.isPlaying) {
        [self.player stop];
        [sender setTitle:@"Play" forState:UIControlStateNormal];
    } else {
        [sender setTitle:@"Stop" forState:UIControlStateNormal];

        NSError *err = nil;
        [self.player play];

        if (self.player == nil) {
            self.player = [[AVAudioPlayer alloc]
                           initWithContentsOfURL:self.audioFileURL
                           error:&err];
            if (err != nil) {
                NSLog(@"Couldn't load player: %@", err.localizedDescription);
                abort();
            }
        }
        [self.player play];
    }
}

@end
